# README #

Small chat application.

### Quick start ###

Note: Redis is required.

* Clone repository
* From my_chat folder run:
pip install -r requirements.txt
* Go to my_chat folder(one with manage.py and server.py in it) and run:
./manage.py migrate 
* Then in one console window run:
./server.py "127.0.0.1:8002"
* and in another one:
./manage.py runserver "127.0.0.1:8001"
* Go to http://127.0.0.1:8001/ in your browser
