(function () {
  'use strict';

  angular
    .module('my_chat.utils', [
      'my_chat.utils.services'
    ]);

  angular
    .module('my_chat.utils.services', []);
})();