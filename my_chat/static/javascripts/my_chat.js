(function () {
  'use strict';

  angular
    .module('my_chat.routes', ['ngRoute', ]);

  angular
    .module('my_chat.config', []);

  angular
    .module('my_chat', [
      'my_chat.config',
      'my_chat.routes',
      'my_chat.authentication',
      'my_chat.layout',
      'my_chat.utils',
      'SwampDragonServices',
    ])
    .run(run);

  run.$inject = ['$http'];

  /**
  * @name run
  * @desc Update xsrf $http headers to align with Django's defaults
  */
  function run($http) {
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';
    $.material.init();
  }
})();

