/**
 * LoginController
 * @namespace my_chat.authentication.controllers
 */
(function () {
    'use strict';

    angular
        .module('my_chat.authentication.controllers')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'Authentication'];

    /**
     * @namespace LoginController
     */
    function LoginController($location, Authentication) {
        var vm = this;

        vm.login = login;
        vm.errors = false;

        activate();

        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf my_chat.authentication.controllers.LoginController
         */
        function activate() {
            // If the user is authenticated, they should not be here.
            if (Authentication.isAuthenticated()) {
                $location.url('/');
            }
        }

        /**
         * @name login
         * @desc Log the user in
         * @memberOf my_chat.authentication.controllers.LoginController
         */
        function login() {
            vm.errors = false;
            Authentication.login(vm.email, vm.password).then(loginSuccess, loginFailed);
        }

        /**
         * @name loginSuccess
         * @desc Catch successful authentication
         * @param data
         * @memberOf my_chat.authentication.controllers.LoginController
         */
        function loginSuccess(data){
            console.log(data);
            //  ToDo

            console.log("login Success!")
        }


        /**
         * @name loginFailed
         * @desc Notify user that authentication failed
         * @param data
         * @memberOf my_chat.authentication.controllers.LoginController
         */
        function loginFailed(data){
            console.log(data);
            vm.errors = true;
            //  ToDo
            console.log("login Failed!")
        }
    }
})();