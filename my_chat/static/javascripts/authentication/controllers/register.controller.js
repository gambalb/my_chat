/**
 * Register controller
 * @namespace my_chat.authentication.controllers
 */
(function () {
    'use strict';

    angular
        .module('my_chat.authentication.controllers')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['$location', '$scope', 'Authentication'];

    /**
     * @namespace RegisterController
     */
    function RegisterController($location, $scope, Authentication) {
        var vm = this;

        vm.register = register;

        activate();

        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf my_chat.authentication.controllers.RegisterController
         */
        function activate() {
            // If the user is authenticated, they should not be here.
            if (Authentication.isAuthenticated()) {
                $location.url('/');
            }
        }

        /**
         * @name register
         * @desc Register a new user
         * @memberOf my_chat.authentication.controllers.RegisterController
         */
        function register() {
            Authentication.register(vm.email, vm.password, vm.username).then(registerSuccess, registerFailed);
        }

        /**
         * @desc Catch successful registration
         * @param data
         * @memberOf my_chat.authentication.controllers.RegisterController
         */
        function registerSuccess(data){
            //  ToDo
            console.log("register Success!")
        }

        /**
         * @desc Notify user that registration failed
         * @param data
         * @memberOf my_chat.authentication.controllers.RegisterController
         */
        function registerFailed(data){
            //  ToDo
            vm.errors = true;

            console.log("register Failed!")
        }



    }
})();