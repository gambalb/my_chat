(function () {
    'use strict';

    angular
        .module('my_chat.authentication.controllers', []);

    angular
        .module('my_chat.authentication.services', ['ngCookies']);

    angular
        .module('my_chat.authentication', [
            'my_chat.authentication.controllers',
            'my_chat.authentication.services'
        ]);
})();