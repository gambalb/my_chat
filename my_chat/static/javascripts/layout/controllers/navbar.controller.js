/**
* NavbarController
* @namespace my_chat.layout.controllers
*/
(function () {
  'use strict';

  angular
    .module('my_chat.layout.controllers')
    .controller('NavbarController', NavbarController);

  NavbarController.$inject = ['$scope', 'Authentication'];

  /**
  * @namespace NavbarController
  */
  function NavbarController($scope, Authentication) {
    var vm = this;
    // vm.isAuthenticated = Authentication.isAuthenticated();

    vm.logout = logout;



    /**
    * @name logout
    * @desc Log the user out
    * @memberOf my_chat.layout.controllers.NavbarController
    */
    function logout() {
      Authentication.logout();
    }
  }
})();