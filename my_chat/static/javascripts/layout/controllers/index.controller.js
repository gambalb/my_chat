/**
* IndexController
* @namespace my_chat.layout.controllers
*/
(function () {
  'use strict';

  angular
    .module('my_chat.layout.controllers')
    .controller('IndexController', IndexController);

  IndexController.$inject = ['$scope', '$location', 'Authentication', 'Snackbar', '$dragon'];

  /**
  * @namespace IndexController
  */
  function IndexController($scope, $location, Authentication, Snackbar, $dragon) {
    var vm = this;

    vm.isAuthenticated = Authentication.isAuthenticated();
    vm.account = Authentication.getAuthenticatedAccount();
    vm.user = undefined;
    if (vm.account) {
      vm.user = vm.account.email;
    }
    vm.posts = [];

    vm.nickname = "";
    vm.message = "";

    vm.channel = 'chat';
    vm.messages = [];

    /// Subscribe to the chat router
    $dragon.onReady(function() {
        $dragon.subscribe('chat-route', vm.channel).then(function(response) {
        });
    });

    $dragon.onChannelMessage(function(channels, message) {
        if (channels.indexOf(vm.channel) > -1) {
            $scope.$apply(function() {
                vm.messages.unshift(message);
            });
        }
    });

    vm.sendMessage = function() {
      if (vm.user != undefined){
        var message_created_at = new Date()
        var message = vm.message;
        vm.message = ""

        var data = {
          name: vm.nickname,
          message: message,
          created_at: message_created_at,
          user: vm.user
        }
        

        $dragon.callRouter('chat', 'chat-route', data).then(function(response) {
        }, function(errors) {
            vm.message = message
            for (var key in errors.errors) {
              if (errors.errors.hasOwnProperty(key)) {
                Snackbar.error(key + " -> " + errors.errors[key]);
              }
            }
            

        });
      }  else {
        Snackbar.error("Only authorized users can send messages. Login and try again.");
      }
      
     }


    activate();

    /**
    * @name activate
    * @desc Actions to be performed when this controller is instantiated
    * @memberOf my_chat.layout.controllers.IndexController
    */
    function activate() {

    }
  }
})();