(function () {
  'use strict';

  angular
    .module('my_chat.layout', [
      'my_chat.layout.controllers'
    ]);

  angular
    .module('my_chat.layout.controllers', []);
})();