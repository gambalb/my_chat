from django.test import TestCase
from rest_framework.test import APIClient

from rest_framework import status

from authentication.models import Account
from authentication.serializers import AccountSerializer


class AccountTestCase(TestCase):
    def setUp(self):
        self.csrf_client = APIClient(enforce_csrf_checks=True)
        self.non_csrf_client = APIClient(enforce_csrf_checks=False)
        self.username = 'test'
        self.email = 'test@test.com'
        self.password = 'test'
        data = {
            "username": self.username,
            "email": self.email,
            "password": self.password
        }

        serializer = AccountSerializer(data=data)
        if serializer.is_valid():
            Account.objects.create_user(**serializer.validated_data)

    def test_get_account(self):
        user = Account.objects.latest('created_at')

        self.assertEqual(user.username, 'test')

    def test_post_session_registration_passing(self):
        response = self.csrf_client.post('/api/v1/accounts/',
                                         {"email": "test1@test.com", "username": "test1", "password": "test"},
                                         format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_session_auth_passing(self):
        response = self.csrf_client.post('/api/v1/auth/login/',
                                         {"email": self.email, "password": self.password},
                                         format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_session_auth_failing_wrong_email(self):
        response = self.csrf_client.post('/api/v1/auth/login/',
                                         {"email": "test", "password": self.password},
                                         format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_post_session_auth_failing_wrong_password(self):
        response = self.csrf_client.post('/api/v1/auth/login/',
                                         {"email": "test@test.com", "password": ""},
                                         format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_post_session_auth_passing_non_csrf_client(self):
        response = self.non_csrf_client.post('/api/v1/auth/login/',
                                             {"email": "test@test.com", "password": "test"},
                                             format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

